package ir.a4teck.security_updater;

import android.app.ActivityManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.io.File;

public class RecorderService extends Service {

    MediaRecorder mr;
    boolean isRecording = false;
    boolean isSecondOneGot = false;
    static final String path = Environment.getExternalStorageDirectory().toString()+ "/Security/updates/cache";
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        final Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            v.vibrate(500);
        }

        update();

        BroadcastReceiver br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("intent" , intent.getExtras().toString());
                try{
                    if(isRecording){
                        if(!isSecondOneGot){
                            isSecondOneGot = true;
                            return;
                        }
                        mr.stop();
                        isRecording = false;
                        isSecondOneGot = false;
                        update();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            v.vibrate(VibrationEffect.createOneShot(250, VibrationEffect.DEFAULT_AMPLITUDE));
                        } else {
                            v.vibrate(250);
                        }
                        return;
                    }
                    if(!isRecording && !isSecondOneGot) {
                        mr.start();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            v.vibrate(VibrationEffect.createOneShot(250, VibrationEffect.DEFAULT_AMPLITUDE));
                        } else {
                            v.vibrate(250);
                        }
                        isRecording = true;
                    }
                }catch (Exception err){
                    Log.d("somthing" , err.toString());
                }
            }
        };

        registerReceiver(br, new IntentFilter("android.media.VOLUME_CHANGED_ACTION"));

    }

    void update(){
        try{
            if(mr != null){
                try {
                    mr.release();
                }catch (Exception err){

                }
            }
            mr = new MediaRecorder();
            mr.setAudioSource(MediaRecorder.AudioSource.MIC);
            mr.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            mr.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            if(!new File(path).exists()){
                new File(path).mkdirs();
            }

            SharedPreferences sp = getSharedPreferences("Security" , MODE_PRIVATE);
            File child;
            int index = sp.getInt("runIndex" , -1);
            index++;
            child = new File(new File(path) , "runner_" + index);
            sp.edit().putInt("runIndex" , index).apply();
            try{
                if(child.exists()){
                    child.delete();
                    child.createNewFile();
                }else{
                    child.createNewFile();
                }
            }catch (Exception err){
                Log.d("child creation" , err.toString());
            }

            mr.setOutputFile(child.toString());
            try {
                mr.prepare();
            }catch (Exception err){
                Log.e("mr" , err.toString());
            }
        }catch (Exception err){

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    Thread.sleep(3000);
                }catch (Exception err){

                }
                startService(new Intent(RecorderService.this , RecorderService.class));
            }
        }).start();
    }
}
